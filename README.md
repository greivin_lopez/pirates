# THE PIRATES PROBLEM #

Five pirates of different ages have a treasure of 100 gold coins.
On their ship, they decide to split the coins using this scheme: 

* The oldest pirate proposes how to share the coins, and ALL pirates (including the oldest) vote for or against it. 

* If 50% or more of the pirates vote for it, then the coins will be shared that way. Otherwise, the pirate proposing the scheme will be thrown overboard, and the process is repeated with the pirates that remain.

* As pirates tend to be a bloodthirsty bunch, **if a pirate would get the same number of coins if he voted for or against a proposal, he will vote against so that the pirate who proposed the plan will be thrown overboard.**

* Assuming that all 5 pirates are intelligent, rational, greedy, and do not wish to die, (and are rather good at math for pirates) what will happen?

# SOLUTION #

Assuming N as the number of pirates and K the number of coins.

For N=2. Let’s say pirate A being the most senior, he would just vote for himself and that would be the majority thus he is going to keep all the money, leaving pirate B with nothing.

For N=3. Pirate A has to one other person to vote for his plan. Pirate A has perfect logic so, he realised that in case his proposal is declined and he is executed pirate C will get nothing (see previous scenario). So, his proposal is to give 1 coin to pirate C and keep (K-1) coins. Pirate C has to vote for his plan or he guaranteed to get nothing. 

The pattern will be repeated for N+1 pirates so the answer is to know how much coins the more senior pirate needs to give a way.

The number of coins the senior pirate needs to give away is Floor((N-1)/2).


### What is this repository for? ###

This repository contains a solution developed in Go. [Click here](http://golang.org/) for more info about Go.
This includes a package containing the solution, a command line program to test the solution on terminal and unit tests to validate solution.

### How do I run the tests? ###

### Run it online ###

Test it online by using the [Go Playground](http://play.golang.org/p/pF8IECATbD).

### Run the command line tool ###

Other option is to download and run the command line tool included **(Binaries works just for Mac OSX)**:

* Download the solution and uncompress it to your Desktop
* Navigate to the folder
* There should be a file without extension (the binary file).


![Screen Shot 2014-05-23 at 5.46.49 PM.png](https://bitbucket.org/repo/XMqKEB/images/23254094-Screen%20Shot%202014-05-23%20at%205.46.49%20PM.png)


* Open a terminal and navigate to the folder
* Give the binary file execution permissions


```
#!shell

$ sudo chmod u+x problem
```

* Run the command line tool.


```
#!shell

$ ./problem
```

* For instructions on how to use it run:


```
#!shell

$ ./problem -help
```

### Compile the source code ###

If you are now using Mac OSX then you must compile the source code.

* Install Go on your system following these instructions: [Go Installation Guideline](http://golang.org/doc/install)
* Navigate to the folder where the source is and run this command


```
#!shell

$ go build problem.go
```

* If it not said anything that means compilation succeed.  Then proceed to run the tool as described in the previous section.

### Example ###

This is an example of compiling and running the command line tool:

```
#!shell

$ go build problem.go
$ ./problem -help
Usage of ./problem:
  -coins=100: How many coins they are in the chest
  -pirates=5: Number of pirates to divide the loot
$ ./problem -pirates=15 coins=500
Testing Propose(15, 100): [93 0 1 0 1 0 1 0 1 0 1 0 1 0 1]
```


### How to run the unit tests ###

* Follow the instruction on how to install Go.
* Navigate to the root folder of the project.
* Now navigate to the **pirates** folder


```
#!shell

$ cd pirates
```

* Run the next command


```
#!shell

$ go test
```

It will run all the tests and let you know the result.