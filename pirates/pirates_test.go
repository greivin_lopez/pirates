package pirates

import (
	"testing"
)

// Test if the given slices are equal
// Unfortunately there is not an equality operator or function
// defined for slices of any kind.
// An possible approach is to use: reflect.DeepEqual()
// Probably faster also.
func equalSlice(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

// Test the Propose function with different scenarios
func TestPirates(t *testing.T) {

	// Edge case Propose(0,0) should throw an error
	pirates := 0
	coins := 0
	_, err := Propose(pirates, coins)
	if err == nil {
		t.Errorf("Not throwing error in Propose(%v, %v): %v", pirates, coins, err)
	}

	// Edge case Propose(1, k) should return [k]
	pirates = 1
	coins = 0
	expected := []int{0}
	proposal, err := Propose(pirates, coins)
	if err != nil {
		t.Errorf("Throwing error in Propose(%v, %v): %v", pirates, coins, err)
	}
	if !equalSlice(proposal, expected) {
		t.Errorf("Invalid response Propose(%v, %v) = %v. Expected: %v", pirates, coins, proposal, expected)
	}

	// Edge case Propose(2, k) should return [k, 0]
	pirates = 2
	coins = 100
	expected = []int{100, 0}
	proposal, err = Propose(pirates, coins)
	if err != nil {
		t.Errorf("Throwing error in Propose(%v, %v): %v", pirates, coins, err)
	}
	if !equalSlice(proposal, expected) {
		t.Errorf("Invalid response Propose(%v, %v) = %v. Expected: %v", pirates, coins, proposal, expected)
	}

	// Propose(n, k) should return [Floor((n-1)/2), 0, 1, ...]
	pirates = 3
	coins = 100
	expected = []int{99, 0, 1}
	proposal, err = Propose(pirates, coins)
	if err != nil {
		t.Errorf("Throwing error in Propose(%v, %v): %v", pirates, coins, err)
	}
	if !equalSlice(proposal, expected) {
		t.Errorf("Invalid response Propose(%v, %v) = %v. Expected: %v", pirates, coins, proposal, expected)
	}

	// Propose(n, k) should return [Floor((n-1)/2), 0, 1, ...]
	pirates = 6
	coins = 1000
	expected = []int{998, 0, 1, 0, 1, 0}
	proposal, err = Propose(pirates, coins)
	if err != nil {
		t.Errorf("Throwing error in Propose(%v, %v): %v", pirates, coins, err)
	}
	if !equalSlice(proposal, expected) {
		t.Errorf("Invalid response Propose(%v, %v) = %v. Expected: %v", pirates, coins, proposal, expected)
	}

	// Propose(n, k) should return [Floor((n-1)/2), 0, 1, ...]
	pirates = 10
	coins = 2580
	expected = []int{2576, 0, 1, 0, 1, 0, 1, 0, 1, 0}
	proposal, err = Propose(pirates, coins)
	if err != nil {
		t.Errorf("Throwing error in Propose(%v, %v): %v", pirates, coins, err)
	}
	if !equalSlice(proposal, expected) {
		t.Errorf("Invalid response Propose(%v, %v) = %v. Expected: %v", pirates, coins, proposal, expected)
	}

}
