// Package pirates will give a solution to the following problem:
// --------------------------------------------------------------
// 5 pirates of different ages have a treasure of 100 gold coins.
//
// On their ship, they decide to split the coins using this scheme:
//
// The oldest pirate proposes how to share the coins, and ALL pirates (including the oldest) vote for or against it.
//
// If 50% or more of the pirates vote for it, then the coins will be shared that way. Otherwise, the pirate proposing the scheme will be thrown overboard, and the process is repeated with the pirates that remain.
//
// As pirates tend to be a bloodthirsty bunch, ===if a pirate would get the same number of coins if he voted for or against a proposal, he will vote against so that the pirate who proposed the plan will be thrown overboard.
//
// Assuming that all 5 pirates are intelligent, rational, greedy, and do not wish to die, (and are rather good at math for pirates) what will happen?
package pirates

import (
	"errors"
	"math"
)

// Propose returns the best proposal for the most senior pirate.
func Propose(numPirates, numCoins int) (proposal []int, err error) {
	if numPirates < 1 {
		return nil, errors.New("Number of pirates should be an integer greater than zero")
	}
	if numCoins < (numPirates-1)/2 {
		return nil, errors.New("Wrong number of coins. Should be at least (n-1)/2 for n pirates")
	}
	if numPirates == 1 {
		return []int{numCoins}, nil
	}
	proposal = make([]int, numPirates)
	proposal[0] = numCoins - int(math.Floor(float64((numPirates-1)/2)))
	for i, _ := range proposal {
		if i > 0 {
			proposal[i] = 0
			if i%2 == 0 {
				proposal[i] = 1
			}
		}
	}
	return proposal, nil
}
