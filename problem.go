package main

import (
	"./pirates"
	"flag"
	"fmt"
)

var (
	numPirates int = 5
	numCoins   int = 100
)

func init() {
	flag.IntVar(&numPirates, "pirates", 5, "Number of pirates to divide the loot")
	flag.IntVar(&numCoins, "coins", 100, "How many coins they are in the chest")
	flag.Parse()
}

func main() {
	if proposal, err := pirates.Propose(numPirates, numCoins); err == nil {
		fmt.Printf("Testing Propose(%v, %v): %v\n", numPirates, numCoins, proposal)
	} else {
		fmt.Printf("Error: %v\n", err)
	}
}
